Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?
    
    The logger.log file contains all normal messages for the initiation of test cases, but does not include stack traces of errors. The console prints both the messages, errors, and stack.
    
1.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']

    That line comes from the class org.junit.jupiter.engine.execution.ConditionEvaluator which checks the tests to ensure they are not disabled, and then prints if they are disabled or not.

1.  What does Assertions.assertThrows do?

    Assertions.assertThrows allows the user to specify the throw that the assertion will throw rather than having it throw the default assert fail or success.

1.  See TimerException and there are 3 questions

        1.  What is serialVersionUID and why do we need it? (please read on Internet)
    
            The serialVersionUID checks and ensures that the attempted load of an object has classes
            that are of the same versionUID to make sure their are no errors in version between updates
         
        2.  Why do we need to override constructors?
        
            You need to override a constructor that is not a default constructor so it knows what super
            class to call, because it is not a default constructor from the extended class.
        
        3.  Why we did not override other Exception methods?
        
            You do not have to override other Exception methods because the default constructor can be
            called automatically without having to question which constructor the constructor is trying
            to call
1.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)

    The static block sets up the logger for the tests, setting the output file and printing warnings if the file was not found, set up correctly, or there was another error.
    
1.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)

    A README.md file format is a file using Markdown for formatting text and describes the project on BitBucket that is being cloned or looked at.
    
1.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)

    The test was failing because, even though an exception was thrown, the class was still running the finally block of the try catch, which was producing a nullPointerException.
    
1.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
    
    The Exceptions are first, the throw for the TimerException, but it is not caught, so it continues. It attempts to catch a InterruptedException and does not get one, then moves to the finally block where it finds the a NullPointerException and catches that automatically.
    
1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel)
1.  Make a printScreen of your eclipse Maven test run, with console
1.  What category of Exceptions is TimerException and what is NullPointerException

    TimerException is a checked error from Exception, while NullPointerException is a runtime error that is unchecked.
    
1.  Push the updated/fixed source code to your own repository.